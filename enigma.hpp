#pragma once
#ifndef ENIGMA_HPP
#define ENIGMA_HPP

#include <utility>
#include <array>
#include <fstream>
#include <string>
#include <iostream>
#include <cctype>
#include <memory>
#include <limits>


class Rotor {

  friend class Enigma;

  std::string ring = "";
  int notch = 0;

  Rotor(int num);

  char operator[](int n) {
    return ring[n];
  }

};

class Plugboard {

  friend class Enigma;
  friend bool validate(std::array<char, 26>&, char);
  friend int count(std::array<char, 26>);

  std::array<char, 26> mappings;

  Plugboard(std::ifstream&);
  Plugboard(bool);
  int scramble(int) const;

};

class Enigma {

  Rotor l_rotor;
  Rotor m_rotor;
  Rotor r_rotor;
  Rotor reflector;

  int l_pos = 0;
  int m_pos = 0;
  int r_pos = 0;


  Plugboard plg;

  bool double_step = false;

public:
  Enigma(int /*lr_num*/ , int /*mr_num*/, int /*rr_num*/, int /*lr_start*/, int /*mr_start*/, int /*rr_start*/, bool /*plug*/);
  Enigma(int /*lr_num*/ , int /*mr_num*/, int /*rr_num*/, int /*lr_start*/, int /*mr_start*/, int /*rr_start*/, std::ifstream&);

  int scramble(int, bool);
  bool cipher_file(std::ifstream&, std::ofstream&, bool);
  void print(void) const;
};


inline int positive_mod(int a, int b) {
  return (b + (a % b)) % b;
}

int find(const std::string&, char);




#endif /* end of include guard:  */
