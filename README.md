# Enigma simulator

A C++ implementation of the 3-rotot German NAVY enigma, with a rotor set of 5, with customizable plugboard.

# USAGE
After compilig run ./enigma [input file].
For the additional settings you will be prompted later.

To decrypt a message, feed it to the program and use the exact same settings.  

For custom  plugboard see the plugboard.txt sample file, you can create your own based on it.

Huge thanks to all the people maintaining online articles about this machine.  

The results from this simulators was compared with other available simulators online.


