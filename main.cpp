#include "enigma.hpp"
#define SUCCESS 0
#define FAILURE -1



int main (int argc, char* argv[] ) {

  if (argc != 2) { std::cout << "Usage: ./enigma <input file>\n"; return FAILURE; }


  std::string out = "out.txt";

  std::ifstream message(argv[1]);
  if (!message) { std::cout << "Invalid file! \n"; return FAILURE; }

  std::cout << " Output filename: ";
  std::cin >> out;


  std::cout << "\nLeft rotor number(1-5): ";
  int lr;
  std::cin >> lr;
  if (!std::cin || lr < 0 || lr > 5) { std::cout << "Bad input!\n"; return FAILURE; }

  std::cout << "\nMid rotor number(1-5 & dont use previous): ";
  int mr;
  std::cin >> mr;
  if (!std::cin || mr < 0 || mr > 5) { std::cout << "Bad input!\n"; return FAILURE; }

  std::cout << "\nRight rotor number(1-5 &  dont use previous): ";
  int rr;
  std::cin >> rr;
  if (!std::cin || rr < 0 || rr > 5) { std::cout << "Bad input!\n"; return FAILURE; }

  if (lr == mr || mr == rr || lr == rr) { std::cout << "Bad input!\n"; return FAILURE; }

  char dummy = ' ';
  std::cout << "\nLeft rotor starting position (A-Z): ";
  std::cin >> dummy;
  if (!std::cin || !std::isalpha(dummy)) { std::cout << "Bad input!\n"; return FAILURE; }
  int ls = static_cast<int>(std::toupper(dummy) -'A');

  std::cout << "\nMid rotor starting position (A-Z): ";
  std::cin >> dummy;
  if (!std::cin || !std::isalpha(dummy)) { std::cout << "Bad input!\n"; return FAILURE; }
  int ms = static_cast<int>(std::toupper(dummy) -'A');

  std::cout << "\nRight rotor starting position (A-Z): ";
  std::cin >> dummy;
  if (!std::cin || !std::isalpha(dummy)) { std::cout << "Bad input!\n"; return FAILURE; }
  int rs = static_cast<int>(std::toupper(dummy) -'A');


  bool sbs = false;
  std::cout << "Do you want steb-by-step simulation? (y/n): ";
  std::cin >> dummy;
  if (dummy == 'y') { sbs = true; }

  std::unique_ptr<Enigma> enigma;
  bool plug = false;
  std::cout << "Do you want to use plugin board? (n-no, y -default, c-custom from file): ";
  std::cin >> dummy;
  if (dummy == 'y') {
    plug = true;
  }

  if (dummy == 'c') {
    std::cout << "Filename: ";
    std::string plugs = "";
    std::cin >> plugs;
    std::ifstream plugboard(plugs);
    enigma = std::make_unique<Enigma>(lr, mr, rr, ls, ms, rs, plugboard);
   }


  if (!enigma) {  enigma = std::make_unique<Enigma>(lr, mr, rr, ls, ms, rs, plug); }
  enigma->print();
  std::ofstream output(out);
  enigma->cipher_file(message, output, sbs);


  return SUCCESS;
}
