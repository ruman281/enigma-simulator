#include "enigma.hpp"


int find(const std::string& ring, char ch) {
  for (int i = 0; i < 26; i++) {
    if (ring[i] == ch) { return i; }
  }
  return -1;
}

int count(std::array<char, 26>& map, char ch) {
  int count = 0;
  for (int i = 0; i < 26; i++) {
    if (map[i] == ch) { count ++; }
  }
  return count;
}

bool validate(std::array<char, 26>& map) {

    for (int i = 0; i < 26; i++) {
      if (count(map, static_cast<char>(i + 'A')) != 1) {
        return false;
      }
      if (map[map[i] - 'A'] != i + 'A' ) { return false; }
    }
    return true;
}

/* predefined rotor wirings, based on Wikipedia */
Rotor::Rotor(int num) {
  switch (num) {
    case 1 :
      ring = "EKMFLGDQVZNTOWYHXUSPAIBRCJ";
      notch = 17;
      break;
    case 2 :
      ring = "AJDKSIRUXBLHWTMCQGZNPYFVOE";
      notch = 5;
      break;
    case 3:
      ring = "BDFHJLCPRTXVZNYEIWGAKMUSQO";
      notch = 22;
      break;
    case 4:
      ring = "ESOVPZJAYQUIRHXLNFTGKDCMWB";
      notch = 10;
      break;
    case 5 :
      ring = "VZBRGITYUPSDNHLXAWMJQOFECK";
      notch = 0;
      break;
    default : // reflector type B
      ring = "YRUHQSLDPXNGOKMIEBFZCWVJAT";
      notch = -5;
  }

}

Plugboard::Plugboard(bool n){
    if (!n) { mappings = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' }; }
    else    { mappings = { 'Q', 'B', 'D', 'C', 'R', 'X', 'T', 'H', 'U', 'L', 'K', 'J', 'O', 'Z', 'M', 'P', 'A', 'E', 'V', 'G', 'I', 'S', 'Y', 'F', 'W', 'N' }; }
}

Plugboard::Plugboard(std::ifstream& inp) {

  inp.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  for (int i = 0; i < 26; i++) {
    if (inp.eof()) {
    std::cerr << "Bad plugboard, encryption without plugboard!\n";
    mappings = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
    return;
    }
    inp >> mappings[i];
  }
  if (!validate(mappings)) {
    std::cout << "Invalid plugs, encryption without plugboard used\n";
    mappings = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
  }
}

int Plugboard::scramble(int ch) const {
  return mappings[ch] - 'A';
}


Enigma::Enigma (int lr_num, int mr_num, int rr_num, int lr_start, int mr_start, int rr_start, bool plug):
  l_rotor(lr_num),
  m_rotor(mr_num),
  r_rotor(rr_num),
  reflector(-2),
  l_pos(lr_start),
  m_pos(mr_start),
  r_pos(rr_start),
  plg(plug) {}

Enigma::Enigma (int lr_num, int mr_num, int rr_num, int lr_start, int mr_start, int rr_start, std::ifstream& plug):
  l_rotor(lr_num),
  m_rotor(mr_num),
  r_rotor(rr_num),
  reflector(-2),
  l_pos(lr_start),
  m_pos(mr_start),
  r_pos(rr_start),
  plg(plug) {}


void Enigma::print(void) const {
  std::cout << "ALPHABET   " << "ABCDEFGHIJKLMNOPQRSTUVWXYZ\n";
  std::cout << "LEFT:      " << l_rotor.ring << "  position: " << static_cast<char>(l_pos + 'A') << '\n';
  std::cout << "MID:       " << m_rotor.ring << "  position: " << static_cast<char>(m_pos + 'A')  << '\n';
  std::cout << "RIGHT:     " << r_rotor.ring << "  position: " << static_cast<char>(r_pos + 'A')  << '\n';
  std::cout << "REFLECTOR: " << reflector.ring << "\n\n\n";

}


int Enigma::scramble(int ch, bool sbs = false) {

  r_pos = (r_pos + 1) % 26;
  if (r_pos == r_rotor.notch || double_step) {
    m_pos = (m_pos + 1) % 26;
    double_step = false;
    if (m_pos == (m_rotor.notch - 1)) {
      double_step = true;
    }
    if (m_pos == m_rotor.notch) {
      l_pos = (l_pos + 1) % 26;
    }
  }
  if (sbs) { print(); }

  ch = plg.scramble(ch);
  if (sbs) { std::cout << static_cast<char>(ch + 'A') << "-"; }

  ch = r_rotor[(ch + r_pos) % 26] - 'A';
  if (sbs) { std::cout << static_cast<char>(ch + 'A') << '-'; }

  ch = m_rotor[positive_mod((ch - r_pos + m_pos), 26)] - 'A';
  if (sbs) { std::cout << static_cast<char>(ch + 'A') << "-"; }

  ch = l_rotor[positive_mod((ch - m_pos + l_pos), 26)] - 'A';
  if (sbs) { std::cout << static_cast<char>(ch + 'A') << "-"; }

  ch = reflector[positive_mod(ch - l_pos, 26)] - 'A';
  if (sbs) { std::cout << static_cast<char>(ch + 'A') << "-"; }

  ch = positive_mod(find(l_rotor.ring, ((ch + l_pos) % 26) + 'A') - l_pos, 26);
  if (sbs) { std::cout << static_cast<char>(ch + 'A') << "-"; }

  ch = positive_mod(find(m_rotor.ring, ((ch + m_pos) % 26) + 'A') - m_pos, 26);
  if (sbs) { std::cout << static_cast<char>(ch + 'A') << "-"; }

  ch = positive_mod(find(r_rotor.ring, ((ch + r_pos) % 26) + 'A') - r_pos, 26);
  if (sbs) { std::cout << static_cast<char>(ch + 'A') << "-" ; }

  ch = plg.scramble(ch);
  if (sbs) { std::cout << static_cast<char>(ch + 'A') << "-"; }

  return ch;

}


bool Enigma::cipher_file(std::ifstream& input, std::ofstream& output, bool sbs /*step-by-step*/) {
  char ch = '\0';
  int ret = 0;
  while (!input.eof()) {
    input >> std::noskipws >>  ch;
    if (input.eof()) { return true; }
    ret = static_cast<int>(ch);
    if (std::isalpha(ch)) {
      ret = scramble(std::toupper(ch) - 'A', sbs);
      ret += 'A';
    }
    output << static_cast<char>(ret);
    if (sbs) { std::cout << "        " << ch << "->" << static_cast<char>(ret) << '\n'; }
  }
  return false;
}
